<?php defined('_ICMS') or die; ?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="An Intelligent Content Management System">
		<meta charset="UTF-8" />
		<!-- jQuery --><script src="/templates/admin/js/jquery-2.2.4.min.js"></script>
		<!-- jQuery Form Plugin --><script src="/templates/admin/js/jquery.form.min.js"></script>
		<!-- FontAwesome --><link rel="stylesheet" href="/templates/admin/css/font-awesome.min.css">
		<!-- CKEditor--><script src="/templates/admin/js/ckeditor/ckeditor.js"></script>
		<!-- Bootstrap CSS --><link rel="stylesheet" href="/templates/admin/css/bootstrap.min.css" >
		<!-- Bootstrap JS --><script src="/templates/admin/js/bootstrap.min.js"></script>
		<!-- Admin JS --><script type="text/javascript" src="/templates/admin/js/main.js"></script>
		<!-- Admin CSS --><link type='text/css' rel='stylesheet' href='/templates/admin/css/style.css' />
		<!-- PNotify JS --><script type="text/javascript" src="/templates/admin/js/pnotify.custom.min.js"></script>
		<!-- PNotify CSS --><link href="/templates/admin/css/pnotify.custom.min.css" media="all" rel="stylesheet" type="text/css" />
		<!-- Favicon --><link href="/templates/admin/favicon.ico" rel="icon" type="image/x-icon" />
		<!-- Parsley JS --><script src="/templates/admin/js/parsley.min.js"></script>

		<title><?php echo $this->settings->production->site->name." - Admin Panel" ?></title>
	</head>
	<body>